// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>

int precarios(int n, int k);

int main()
{
    int k;
    std::cin >> k;
    while (std::cin) {
        std::cout << precarios(k) << std::endl;
        std::cin >> k;
    }
}

int precarios(int k) {
    int hijos, prec = 0;
    std::cin >> hijos;
    if (hijos == 0) return k >= 0;
    for (int i = 0; i < hijos; i++)
        prec += precarios(--k);
    return prec;
}

