#include "bintree_eda.h"
#include <iostream>
#include <fstream>
using namespace std;

bool esSimetrico(bintree<char> const& izq, bintree<char> const &der) {
    if (izq.empty() && der.empty()) return true;
    else if (izq.empty() && !der.empty() || !izq.empty() && der.empty()) return false;
    return esSimetrico(izq.left(), der.right()) && esSimetrico(izq.right(), der.left());
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; i++) {
       bintree<char> arbol = leerArbol('.');
       std::cout << (esSimetrico(arbol.left(), arbol.right()) ? "SI" : "NO") << "\n";
   }
}