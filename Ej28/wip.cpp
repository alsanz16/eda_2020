#include <iostream>
#include <fstream>
#include <map>
#include <string>
using namespace std;

int convertirPuntuacion(std::string s) {
    if (s == "CORRECTO") return 1;
    else return -1;
}

void resolverCaso(int num) {
    std::string nombre;
    std::string puntuacion;
    std::map<std::string, int> puntuaciones;
    for (int i = 0; i < num; i++) {
        std::getline(std::cin, nombre);
        std::getline(std::cin, nombre);
        std::cin >> puntuacion;
        puntuaciones[nombre] += convertirPuntuacion(puntuacion);
    }
    for (auto it = puntuaciones.begin(); it != puntuaciones.end(); ++it) {
        if (it->second) std::cout << it->first << ", " << it->second << "\n";
    }
    std::cout << "---\n";
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   while (numCasos) {
       resolverCaso(numCasos);
       std::cin >> numCasos;
   }
}