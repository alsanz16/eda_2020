//
//  ordered_list.h
//  wip
//
//  Created by Álvaro on 24/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef ordered_list_h
#define ordered_list_h

#include "queue.h"

template <typename T>
class ordered_queue : public queue<T> {
using Node = typename queue<T>::Nodo;   
public:
    ordered_queue() {};
    void insert(ordered_queue<T> &q) {
        Node *t1 = this->prim, *t2 = q.prim;
        Node t, *current = &t;
        
        while (t1 != nullptr && t2 != nullptr) {
            if (t1->elem < t2->elem) {
                current->sig = t1;
                t1 = t1->sig;
            }
            else {
                current->sig = t2;
                t2 = t2->sig;
            }
            current = current->sig;
        }
        
        if (t1 == nullptr) {
            current->sig = t2;
            this->ult = q.ult;
        }
        else current->sig = t1;
        
        this->nelems += q.nelems;
        this->prim = t.sig;
        q.prim = q.ult = nullptr;
        q.nelems = 0;
    }
};


#endif /* ordered_list_h */
