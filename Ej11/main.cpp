// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include "ordered_queue.h"

template <typename T>
void print(queue<T> q);

int main()
{
    int casosPrueba, elementoLeido;
    
    std::cin >> casosPrueba;
    for (int i = 0; i < casosPrueba; i++) {
        ordered_queue<int> q1, q2;
        std::cin >> elementoLeido;
        while(elementoLeido != 0) {
            q1.push(elementoLeido);
            std::cin >> elementoLeido;
        }
        
        std::cin >> elementoLeido;
        while(elementoLeido != 0) {
            q2.push(elementoLeido);
            std::cin >> elementoLeido;
        }
        
        q1.insert(q2);
        std::cout << q1 << std::endl;
    }
}
