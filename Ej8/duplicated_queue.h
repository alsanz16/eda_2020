//
//  Lista.h
//  wip
//
//  Created by Álvaro on 24/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Lista_h
#define Lista_h

#include "queue.h"

template <typename T>
class duplicated_queue : public queue<T> {
using Node = typename queue<T>::Nodo;
public:
    duplicated_queue() {};
    void duplicate() {
        Node *current = this->prim;
        while (current != nullptr) {
            current -> sig = new Node(current->elem, current->sig);
            current = current->sig->sig;
        }
        this->nelems *= 2;
        this->ult = this->ult->sig;
    }
};


#endif /* Lista_h */
