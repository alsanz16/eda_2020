// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include "duplicated_queue.h"

template <typename T>
void print(queue<T> q);

int main()
{
    int elementoLeido;
    std::cin >> elementoLeido;
    while (std::cin) {
        duplicated_queue<int> cola;
        while(elementoLeido) {
            cola.push(elementoLeido);
            std::cin >> elementoLeido;
        }
        cola.duplicate();
        std::cout << cola  << std::endl;
        std::cin >> elementoLeido;
    }
}
