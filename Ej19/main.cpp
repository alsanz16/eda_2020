// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "bintree_eda.h"

typedef struct {
    unsigned int nodos = 0, altura = 0, hojas = 0;
} tInfo;

template <class T>
tInfo datosArbol(bintree<T> const& arbol) {
    if (arbol.empty()) return {0, 0, 0};
    auto hijoIzquierdo = datosArbol(arbol.left());
    auto hijoDerecho = datosArbol(arbol.right());
    return {  1 + hijoIzquierdo.nodos + hijoDerecho.nodos,
        1 + std::max(hijoIzquierdo.altura, hijoDerecho.altura),
        hijoIzquierdo.hojas + hijoDerecho.hojas + (!hijoIzquierdo.altura && !hijoDerecho.altura) };
}

int main()
{
    int c;
    std::cin >> c;
    for (int i = 0; i < c; i++) {
        bintree<char> arbol = leerArbol('.');
        tInfo datos = datosArbol(arbol);
        std::cout << datos.nodos << " " << datos.hojas << " " << datos.altura << std::endl;
    }
    return 0;
}
