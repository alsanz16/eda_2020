// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "bintree_eda.h"

template <class T>
void fronteraArbol(bintree<T> const& arbol, std::vector<T> &v);

template <class T>
void fronteraArbol(bintree<T> const& arbol, std::vector<T> &v) {
    if (arbol.empty()) return;
    if (arbol.right().empty() && arbol.left().empty()) v.push_back(arbol.root());
    fronteraArbol(arbol.left(), v);
    fronteraArbol(arbol.right(), v);
}

int main()
{
    int c;
    std::cin >> c;
    for (int i = 0; i < c; i++) {
        std::vector<int> v;
        bintree<int> arbol = leerArbol(-1);
        fronteraArbol(arbol, v);
        for(auto t = v.begin(); t != v.end(); ++t) std::cout << *t << " ";
        std::cout << std::endl;
    }
    return 0;
}
