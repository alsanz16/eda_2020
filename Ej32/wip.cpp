#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

bool resolverCaso() {
    std::map<std::string, std::set<std::string>> numeroDeportes;
    std::map<std::string, std::string> preferencias;

    std::string deporte, elemento;
    std::cin >> deporte;
    if (!std::cin) return false;

    while (deporte != "_FIN_") {
        numeroDeportes[deporte] = std::set<std::string>();
        std::cin >> elemento;
        while (!isupper(elemento.at(0)) && elemento != "_FIN_") {
            if (preferencias.count(elemento) && preferencias[elemento] != deporte)
                numeroDeportes[preferencias[elemento]].erase(elemento); 
            else {
                numeroDeportes[deporte].insert(elemento);
                preferencias[elemento] = deporte;
            }
            std::cin >> elemento;
        }
        deporte = elemento;
    }
    
    std::map<int, std::set<std::string>, std::greater<int>> fin;
    for (auto it = numeroDeportes.begin(); it != numeroDeportes.end(); ++it)
        fin[it->second.size()].insert(it->first);

    for (auto it = fin.begin(); it != fin.end(); ++it)
        for (auto it2 = it->second.begin(); it2 != it->second.end(); ++it2)
            std::cout << *it2 << " " << it->first << "\n";
    std::cout << "---\n";
    return true;
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   
   while (std::cin) resolverCaso();

}