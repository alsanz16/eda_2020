#include <iostream>
#include "bintree_eda.h"

typedef struct {
    int numEquipos, maxRuta;
} tInfo;

tInfo excursionistas(bintree<int> const& arbol) {
    if (arbol.empty()) return { 0,0 };
    tInfo izq = excursionistas(arbol.left());
    tInfo der = excursionistas(arbol.right());
    int numEquipos = !(izq.numEquipos && der.numEquipos) ?
        std::max(izq.numEquipos, der.numEquipos) :
        izq.numEquipos + der.numEquipos;
    if (!numEquipos) numEquipos = bool(arbol.root());
    return {
        numEquipos,
        arbol.root() + std::max(izq.maxRuta, der.maxRuta)
    };
}

int main()
{
    int numCasos;
    std::cin >> numCasos;
    for (int i = 0; i < numCasos; i++) {
        tInfo info = excursionistas(leerArbol(-1));
        std::cout << info.numEquipos << " " << info.maxRuta << "\n";
    }
}
