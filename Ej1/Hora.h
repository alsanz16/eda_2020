#pragma once
#include <cassert>
#include <iomanip>
#include <iostream>
class Hora {
private:
    int hora_, minuto_, segundo_;
    bool valida() {
        return 0 <= hora_ && hora_ < 24
            && 0 <= minuto_ && minuto_ < 60
            && 0 <= segundo_ && segundo_ < 60;
    }
public:
    Hora() : hora_(0), minuto_(0), segundo_(0) { ; }
    Hora(int hh, int mm, int ss) : hora_(hh), minuto_(mm), segundo_(ss) { comprobar(); }
    void comprobar() { if (!valida()) throw std::domain_error("Datos invalidos"); }
    void set(int hh, int mm, int ss) {
        hora_ = hh;
        minuto_ = mm;
        segundo_ = ss;
        comprobar();
    }
    int hora() const { return hora_; }
    int minuto() const { return minuto_; }
    int segundo() const { return segundo_; }
    bool operator<(const Hora& o2) {
        return hora_ < o2.hora_ ||
            hora_ == o2.hora_ && minuto_ < o2.minuto_ ||
            hora_ == o2.hora_ && minuto_ == o2.minuto_ && segundo_ < o2.segundo_;
    }
};

inline std::istream& operator>>(std::istream &in, Hora &h1) {
    int hora_, minuto_, segundo_;
    char aux;
    in >> hora_ >> aux >> minuto_ >> aux >> segundo_;
    h1.set(hora_, minuto_, segundo_);
    return in;
}

inline std::ostream& operator<<(std::ostream& out, const Hora& h1) {
    out << std::setfill('0') << std::setw(2) << h1.hora() << ":" << std::setw(2) << h1.minuto()
        << ":" << std::setw(2) << h1.segundo() << "\n";
    return out;
}
