// Ejercicio1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>
#include "Hora.h"

template<class T>
bool busquedaBinaria(std::vector<T>& v, T elem, int& pos);

int main()
{
    int numTrenes, numConsultas, pos;
    std::cin >> numTrenes >> numConsultas;
    while (numTrenes + numConsultas != 0) {
        std::vector<Hora> trenes(numTrenes);
        for (Hora& h : trenes) std::cin >> h;
        for (int i = 0; i < numConsultas; i++) {
            Hora h;
            try { std::cin >> h; }
            catch (std::domain_error & e) { std::cout << "ERROR\n"; continue; };
            bool encontrado = busquedaBinaria(trenes, h, pos);
            if (!encontrado && pos == trenes.size()) std::cout << "NO\n";
            else std::cout << trenes[pos];
        }
        std::cout << "---\n";
        std::cin >> numTrenes >> numConsultas;
    }
}

template<class T>
bool busquedaBinaria(std::vector<T>& v, T elem, int &pos) {
    int ini = 0, fin = v.size() - 1, med;
    while (ini <= fin) {
        pos = med = (ini + fin) / 2;
        if (v[med] < elem) ini = med+1;
        else if (elem < v[med]) fin = med-1;
        else return true;
    }
    pos = ini;
    return false;
}
