#include "bintree_eda.h"
#include <iostream>
#include <fstream>
#include "set_eda.h"
using namespace std;

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos, preguntas, aux;
   std::cin >> numCasos;
   while (numCasos) {
       set<int> set;
       for (int i = 0; i < numCasos; i++) {
           std::cin >> aux;
           set.insert(aux);
       }
       std::cin >> preguntas;
       for (int i = 0; i < preguntas; i++) {
           std::cin >> aux;
           std::pair<bool, int> respuesta = set.lower_bound(aux);
           if (respuesta.first) std:cout << respuesta.second << "\n";
           else std::cout << "NO HAY \n";
       }
       std::cout << "---\n";
       std::cin >> numCasos;
       
   }
}