// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>

int main()
{
    std::deque<int> numeros;
    int numLeido, longitud;
    
    std::cin >> longitud;
    while (longitud != 0) {
        for (int c = 0; c < longitud; c++ ) {
            std::cin >> numLeido;
            if (numLeido < 0) numeros.push_front(numLeido);
            else numeros.push_back(numLeido);
        }
        while(!numeros.empty()) {
            std::cout << numeros.front() << " ";
            numeros.pop_front();
        }
        std::cout << std::endl;
        std::cin >> longitud;

    }
    
}

