#include "cola_eliminar.h"
#include "Hora.h"
#include <iostream>
#include <fstream>
using namespace std;


template <typename T>
void imprimir(queue<T> &cola) {
    for (int i = 0; i < cola.size(); i++) {
        std::cout << cola.front() << " ";
        cola.push(cola.front());
        cola.pop();
    }
    std::cout << "\n";
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numDatos = 1;
   Hora aux;
   while (numDatos != 0) {
       std::cin >> numDatos;
       cola_eliminar<Hora> cola;
       for (int i = 0; i < numDatos; i++) {
           std::cin >> aux;
           cola.push(aux);
       }
       cola.eliminar();
       imprimir(cola);
   }
}