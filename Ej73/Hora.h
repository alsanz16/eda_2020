#pragma once
#include <cassert>
#include <iomanip>
#include <iostream>
class Hora {
private:
    int _hora, _minuto, _segundo;
    bool valida() {
        return 0 <= _hora && _hora < 24
            && 0 <= _minuto && _minuto < 60
            && 0 <= _segundo && _segundo < 60;
    }
public:
    Hora() : _hora(0), _minuto(0), _segundo(0) { ; }
    Hora(int hh, int mm, int ss) : _hora(hh), _minuto(mm), _segundo(ss) { comprobar(); }
    void comprobar() { if (!valida()) throw std::domain_error("Datos invalidos"); }
    void set(int hh, int mm, int ss) {
        _hora = hh;
        _minuto = mm;
        _segundo = ss;
        comprobar();
    }
    int hora() const { return _hora; }
    int minuto() const { return _minuto; }
    int segundo() const { return _segundo; }
    bool operator<(const Hora& o2) {
        return _hora < o2._hora ||
            (_hora == o2._hora && _minuto < o2._minuto) ||
            (_hora == o2._hora && _minuto == o2._minuto && _segundo < o2._segundo);
    }

    bool operator==(const Hora& o2) {
        return _hora == o2._hora && _minuto == o2._minuto && _segundo == o2._segundo;
    }
};

inline std::istream& operator>>(std::istream& in, Hora& h1) {
    int _hora, _minuto, _segundo;
    char aux;
    in >> _hora >> aux >> _minuto >> aux >> _segundo;
    h1.set(_hora, _minuto, _segundo);
    return in;
}

inline std::ostream& operator<<(std::ostream& out, const Hora& h1) {
    out << std::setfill('0') << std::setw(2) << h1.hora() << ":" << std::setw(2) << h1.minuto()
        << ":" << std::setw(2) << h1.segundo();
    return out;
}

inline Hora operator+(const Hora& o1, const Hora& o2) {
    int ss = o1.segundo() + o2.segundo();
    int mm = o1.minuto() + o2.minuto() + ss / 60;
    int hh = o1.hora() + o2.hora() + mm / 60;
    return Hora(hh, mm % 60, ss % 60);
}


