#pragma once
#include "queue_eda.h"
template <typename T>
class cola_eliminar : public queue<T> {
	using Nodo = typename queue<T>::Nodo;
public:
	void eliminar() {
		if (this->prim == nullptr || this->prim->sig == nullptr) return;
		Nodo* current = this->prim->sig, * anterior = this->prim, *aux;
		while (current != nullptr) {
			if (current->elem < anterior->elem) {
				anterior->sig = current->sig;
				aux = current->sig;
				delete current;
				this->nelems--;
				current = aux;
			}
			else {
				anterior = current;
				current = current->sig;
			}
		}
		this->ult = anterior;
	}
};