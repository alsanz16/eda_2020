// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "bintree_eda.h"

template <class T>
T minimo(bintree<T> const& arbol);

template <class T>
T minimo(bintree<T> const& arbol) {
    auto it = arbol.begin();
    T min = *it;
    for (++it; it != arbol.end(); ++it)
        min = std::min(*it, min);
    return min;
}

int main()
{
    std::string tipo;
    std::cin >> tipo;
    while (std::cin) {
        if (tipo == "N") std::cout << minimo(leerArbol(-1)) << '\n';
        else if (tipo == "P") std::cout << minimo(leerArbol((std::string) "#")) << '\n';
        std::cin >> tipo;
    }
}
