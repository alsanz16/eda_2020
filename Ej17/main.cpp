// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>

int longitud();

int main()
{
    int casos;
    std::cin >> casos;
    for(int c = 0; c < casos; c++) std::cout << longitud() << std::endl;
}

int longitud() {
    int hijos, max = 0;
    std::cin >> hijos;
    if (hijos == 0) return 1;
    for (int j = 0; j < hijos; j++)
        max = std::max(max, longitud());
    return 1 + max;
}

