#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list> 
#include <string>
#include <sstream>
#include <unordered_map>
#include "bintree_eda.h"
#include "queue_eda.h"
#include "deque_eda.h"

template <typename T>
void imprimir(queue<T> cola) {
    for (int i = 0; i < cola.size(); i++) {
        std::cout << cola.front() << " ";
        cola.push(cola.front());
        cola.pop();
    }
    std::cout << "\n";
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("casos.in");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
    
    int numDatos = 1, aux;
    while (numDatos != 0) {
        std::cin >> numDatos;
        queue<int> cola;
        for (int i = 0; i < numDatos; i++) {
            std::cin >> aux;
            cola.push(aux);
        }
        cola.intercambiar();
        imprimir(cola);
    }

}