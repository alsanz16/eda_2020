#pragma once
#include "queue_eda.h"
template <typename T>
class cola_eliminar : public queue<T> {
	using Nodo = typename queue<T>::Nodo;
public:
	void eliminar() {
		if (this->prim == nullptr) return;
		Nodo *current = this->prim, *aux;
		while (current != nullptr && current->sig != nullptr) {
			aux = current->sig->sig;
			delete current->sig;
			this->nelems--;
			if (aux == nullptr) 
				this->ult = current;
			current->sig = aux;
			current = aux;
		}
	}
};