#include "bintree_eda.h"
#include <iostream>
#include <fstream>
using namespace std;

typedef struct {
    bool esCompleto;
    bool esSemicompleto;
    int altura;
} tInfo;

tInfo esCompleto(bintree<char> const& arbol) {
    if (arbol.empty()) return { true, true, 0};

    tInfo izq = esCompleto(arbol.left());
    tInfo der = esCompleto(arbol.right());
    return { 
        izq.esCompleto && der.esCompleto && izq.altura == der.altura,
        izq.esCompleto && der.esSemicompleto && der.altura == izq.altura
            || izq.esSemicompleto && der.esCompleto && izq.altura == der.altura+1,
        1 + std::max(izq.altura, der.altura) };
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; i++) {
       tInfo info = esCompleto(leerArbol('.'));
       if (info.esCompleto) std::cout << "COMPLETO\n";
       else if (info.esSemicompleto) std::cout << "SEMICOMPLETO\n";
       else std::cout << "NADA\n";
   }
}