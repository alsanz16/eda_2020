// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>
#include "bintree_eda.h"

typedef struct {
    int max, longitud;
} tInfo;

template <class T>
tInfo diam(bintree<T> const& arbol) {
    if (arbol.empty()) return {0, 0};
    tInfo izq = diam(arbol.left());
    tInfo der = diam(arbol.right());
    return { std::max(std::max(izq.max, der.max), 1+izq.longitud+der.longitud),
        1 + std::max(izq.longitud, der.longitud)};
}



int main()
{
    int n;
    std::cin >> n;
    for (int i = 0; i < n; ++i)
        std::cout << diam(leerArbol('.')).max << '\n';
}
