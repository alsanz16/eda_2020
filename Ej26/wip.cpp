#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list> 
#include <string>
#include <sstream>
#include <unordered_map>
#include "bintree_eda.h"
#include "queue_eda.h"

typedef struct {
    bool esBusqueda;
    int max;
    int min;
} tInfo;

tInfo esArbolBinario(bintree<int> arbol) {
    if (arbol.empty()) return { true, INT_MIN, INT_MAX };
    tInfo izq = esArbolBinario(arbol.left());
    tInfo der = esArbolBinario(arbol.right());
    return {
        arbol.root() > izq.max && arbol.root() < der.min
        && izq.esBusqueda && der.esBusqueda,
        std::max(std::max(izq.max, der.max), arbol.root()),
        std::min(std::min(izq.min, der.min), arbol.root())
    };
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("casos.in");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    
    int casos;
    std::cin >> casos;
    for (int i = 0; i < casos; i++) {
        tInfo info = esArbolBinario(leerArbol(-1));
        std::cout << (info.esBusqueda ? "SI" : "NO") << "\n";
    }
}