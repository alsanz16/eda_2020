#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list> 
#include <string>
#include <sstream>
#include <unordered_map>
#include "bintree_eda.h"
#include "queue_eda.h"

template <typename T>
struct tInfo {
    bintree<T> arbol;
    int preorder;
    tInfo(bintree<T> _arbol, int _preorder) : arbol(_arbol), preorder(_preorder) {};
} ;

std::vector<int> leerLinea() {
    std::vector<int> v;
    int aux;

    std::stringstream tmp;
    std::string s;
    std::getline(std::cin, s);
    tmp << s;

    tmp >> aux;
    while (tmp) {
        v.push_back(aux);
        tmp >> aux;
    }
    return v;
}

template <typename T>
tInfo<T> create(std::vector<T>& preorder,
    std::vector<T>& inorder,
    int preorder_start,
    int inorder_start,
    int inorder_end,
    std::unordered_map<T, int>& set) {


    if (inorder_start > inorder_end) return { {}, preorder_start };
    int raiz = preorder.at(preorder_start++);
    if (inorder_start == inorder_end) return { {raiz}, preorder_start };

    tInfo<T> izq = create(preorder, inorder, preorder_start, inorder_start, set.at(raiz) - 1, set);
    tInfo<T> right = create(preorder, inorder, izq.preorder, set.at(raiz) + 1, inorder_end, set);
    return { { izq.arbol, raiz, right.arbol }, right.preorder };
}

template <typename T>
bintree<T> create_tree(std::vector<T> preorder,
    std::vector<T> inorder) {
    std::unordered_map<T, int> m;
    for (int i = 0; i < inorder.size(); i++)
        m.emplace(inorder.at(i), i);
    int preorder_start = 0;
    return create(preorder, inorder, preorder_start, 0, inorder.size() - 1, m).arbol;
}


int main()
{
#ifndef DOMJUDGE
    std::ifstream in("casos.in");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    std::vector<int> preV, inoV;
    do {
        preV = leerLinea();
        inoV = leerLinea();
        bintree<int> arbol = create_tree(preV, inoV);
        for (auto& i : arbol.postorder())
            std::cout << i << " ";
        std::cout << "\n";
    } while (preV.size());
}