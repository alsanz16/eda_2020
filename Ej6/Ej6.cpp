// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

int buscar(const std::vector<char>& v, char elem);

int main()
{
    std::vector<char> abiertos = { '{', '(', '[' };
    std::vector<char> cerrados = { '}', ')', ']' };
    std::string leido;
    int pos;
    
    std::getline(std::cin, leido);
    while (std::cin) {
        std::stack<char> simbolos;
        bool correcto = true;
        for (char caracterLeido : leido) {
            if (buscar(abiertos, caracterLeido) != -1) simbolos.push(caracterLeido);
            else if ((pos = buscar(cerrados, caracterLeido)) != -1) {
                if (simbolos.empty() || simbolos.top() != abiertos[pos]) correcto = false;
                if (!simbolos.empty()) simbolos.pop();
            }
        }
        correcto = correcto && simbolos.empty();
        std::cout << (correcto ? "SI" : "NO") << std::endl;
        std::getline(std::cin, leido);

    }
}

int buscar(const std::vector<char>& v, char elem) {
    int i = 0;
    bool encontrado = false;
    while (!encontrado && i < v.size())
        if (elem == v[i]) encontrado = true;
        else i++;
    return encontrado ? i : -1;
}
