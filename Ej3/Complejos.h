#ifndef Complejos_h
#define Complejos_h
#include <iostream>
#include "math.h"
template <class T>
class Complejo {
private:
    T _real, _imaginaria;
public:
    T real() const { return _real; };
    T imaginaria() const { return _imaginaria; };
    Complejo(T real, T imaginaria) : _real(real), _imaginaria(imaginaria){ };
    Complejo() : _real(0), _imaginaria(0) {} ;
    void set(T real, T imaginaria) {_real = real, _imaginaria = imaginaria;};
    int mod() {return sqrt(_real*_real + _imaginaria*_imaginaria); };
};

template <class T>
inline Complejo<T> operator*(const Complejo<T> &c1, const Complejo<T> &c2) {
    return Complejo<T>(c1.real()*c2.real() - c1.imaginaria()*c2.imaginaria(),
                    c1.real()*c2.imaginaria() + c1.imaginaria()*c2.real());
}

template <class T>
inline Complejo<T> operator+(const Complejo<T> &c1, const Complejo<T> &c2) {
    return Complejo<T>(c1.real() + c2.real(), c1.imaginaria() + c2.imaginaria());
}

template <class T>
inline std::istream& operator>>(std::istream &in, Complejo<T> &c1) {
    T real, imaginaria;
    std::cin >> real >> imaginaria;
    c1.set(real, imaginaria);
    return in;
}


#endif /* Complejos_h */
