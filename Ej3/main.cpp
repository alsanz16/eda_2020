// Ejercicio1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <algorithm>
#include <iostream>
#include <vector>
#include "Complejos.h"

int main()
{
    int casos, iteraciones, i;
    const int limite = 2;
    std::cin >> casos;
    for (int caso = 0; caso < casos; caso++) {
        Complejo<float> c, zn;
        i = 0;
        std::cin >> c >> iteraciones;
        while(i < iteraciones && zn.mod() <  limite) {
            zn = zn*zn + c;
            i++;
        }
        if (i < iteraciones) std::cout << "NO\n";
        else std::cout << "SI\n";
    }
}
