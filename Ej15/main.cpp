// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <set>
#include <stack>
#include <string>
#include <vector>

int main() {
    long int n, k, elementoLeido, j;
    
    std::cin >> n >> k;
    while (!(n == 0 && k == 0)) {
        std::queue<long int> sobres;
        std::multiset<long int, std::greater<long int>> elementos;

        for (j = 0; j < k; j++) {
            std::cin >> elementoLeido;
            elementos.insert(elementoLeido);
            sobres.push(elementoLeido);
        }
        std::cout << *elementos.begin() << " ";

        for (; j < n; j++) {
            std::cin >> elementoLeido;
            auto it = elementos.find(sobres.front());
            if (it != elementos.end()) elementos.erase(it);
            sobres.pop();
            elementos.insert(elementoLeido);
            sobres.push(elementoLeido);
            std::cout << *elementos.begin() << " ";
        }
        
        std::cout << std::endl;
        std::cin >> n >> k;

    }
    
}
