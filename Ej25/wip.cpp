#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list> 
#include <string>
#include <sstream>
#include <unordered_map>
#include "bintree_eda.h"
#include "queue_eda.h"

typedef struct {
    int num;
    int altura;
} tInfo; 

tInfo accesible(bintree<int> const& arbol, int altura, std::vector<bool> &esPrimo) {
    if (arbol.empty()) return { 0, INT_MAX };

    tInfo izq = accesible(arbol.left(), altura + 1, esPrimo );
    tInfo der = accesible(arbol.right(), altura + 1, esPrimo );

    if (esPrimo.at(arbol.root())) return { 0, INT_MAX };
    else if (arbol.root() % 7 == 0) return { arbol.root(), altura };
    else
        if (izq.altura <= der.altura) return izq;
        else return der;
}

int main()
{
#ifndef DOMJUDGE
    std::ifstream in("casos.in");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    const int numMax = 5000;
    std::vector<bool> esPrimo(numMax, true);
    int limit = sqrt(numMax) + 1;
    for (int i = 2; i < limit; i++) {
        int j = 2;
        int result = j * i;
        while (result < numMax) {
            esPrimo.at(result) = false;
            result = ++j * i;
        }
    }
    int casos;
    std::cin >> casos;
    for (int i = 0; i < casos; i++) {
        tInfo info = accesible(leerArbol(-1), 1, esPrimo);
        if (info.num == 0) std::cout << "NO HAY\n";
        else std::cout << info.num << " " << info.altura << "\n";
    }
}