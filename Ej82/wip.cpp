#include "bintree_eda.h"
#include <iostream>
#include <fstream>
using namespace std;

typedef struct {
    int caudal;
    int tramosNavegables;
} tInfo;

const int caudal_max = 3;
tInfo navegable(bintree<int> const& arbol) {
    if (arbol.empty()) return { 0, 0 };
    if (arbol.left().empty() && arbol.right().empty()) return { 1, 0 };
    tInfo izq = navegable(arbol.left());
    tInfo der = navegable(arbol.right());
    int nuevoCaudal = std::max(izq.caudal + der.caudal - arbol.root(), 0);
    return { nuevoCaudal,
        izq.tramosNavegables + der.tramosNavegables + (nuevoCaudal >= caudal_max) };
}

int numAfluentes(bintree<int> const& arbol) {
    //corregimos por si hemos contado la raiz
    tInfo info = navegable(arbol);
    return info.tramosNavegables - (info.caudal >= caudal_max);
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; i++)
       std::cout << numAfluentes(leerArbol(-1)) << "\n";
}