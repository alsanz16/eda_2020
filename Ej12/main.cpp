// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#include <vector>

int main()
{
    int k, n;
    std::cin >> n >> k;
    while (!(k == 0 && n == 0)) {
        int i = 1;
        std::queue<int> alumnos;
        for (int j = 1; j <= n; j++) alumnos.push(j);
        while(alumnos.size() != 1) {
            if (i % (k+1) != 0) alumnos.push(alumnos.front());
            alumnos.pop ();
            i++;
        }
        std::cout << alumnos.front() << std::endl;
        std::cin >> n >> k;

    }
}
