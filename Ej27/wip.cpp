#include <climits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <list> 
#include <string>
#include <sstream>
#include <unordered_map>
#include "bintree_eda.h"
#include "queue_eda.h"

template <typename T>
struct tInfo {
    bintree<T> arbol;
    int preorder;
    tInfo(bintree<T> _arbol, int _preorder) : arbol(_arbol), preorder(_preorder) {};
} ;

std::vector<int> leerLinea() {
    std::vector<int> v;
    int aux;

    std::stringstream tmp;
    std::string s;
    std::getline(std::cin, s);
    tmp << s;

    tmp >> aux;
    while (tmp) {
        v.push_back(aux);
        tmp >> aux;
    }
    return v;
}

template <typename T>
tInfo<T> create(std::vector<T>& preorder,
    int preorder_start,
    int inorder_start,
    int inorder_end,
    std::unordered_map<T, int>& set) {


    if (inorder_start > inorder_end) return { {}, preorder_start };
    int index = preorder_start;
    int raiz = preorder.at(preorder_start++);
    if (inorder_start == inorder_end) return { {raiz}, preorder_start };

    tInfo<T> izq = create(preorder, preorder_start, preorder_start, set.at(index) - 1, set);
    tInfo<T> right = create(preorder, izq.preorder, set.at(index), inorder_end, set);
    return { { izq.arbol, raiz, right.arbol }, right.preorder };
}

template <typename T>
bintree<T> create_tree(std::vector<T> preorder) {
    std::unordered_map<T, int> m;
    for (int i = 0; i < preorder.size(); i++) {
        int j = i+1;
        while (j < preorder.size() && preorder.at(i) > preorder.at(j))
            j++;
        m.emplace(i, j);
    }
    return create(preorder, 0, 0, preorder.size() - 1, m).arbol;
}


int main()
{
#ifndef DOMJUDGE
    std::ifstream in("casos.in");
    auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

    std::vector<int> preV;
    do {
        preV = leerLinea();
        bintree<int> arbol = create_tree(preV);
        for (auto& i : arbol.postorder())
            std::cout << i << " ";
        std::cout << "\n";
    } while (preV.size());
}