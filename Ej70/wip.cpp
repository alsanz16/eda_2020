
// Enrique Rey Gisbert
// Alvaro Sanz Ramos
/*
COSTE en tiempo: 
    O(n) donde n es el numero de nodos de la lista entrante (la que no se engorda).
    En cada iteracion dentro de la funcion *engordar* eliminamos un elemento de la lista *ch*
    y lo insertamos en nuestra lista (por el principio y el final alternativamente).


*/

#include <iostream>
#include <fstream>
using namespace std;

#include "deque_eda.h"

class EngordarLista : public deque<int> {
   using Nodo = deque<int>::Nodo;
public:
   // imprime la lista enlazada
   void print(std::ostream & o = std::cout) const {
       Nodo* it = this->fantasma->sig;
       while (it != this->fantasma) {
           o << it->elem << " ";
           it = it->sig;
       }
   }
   
   // introduce los elementos de ch (una lista enlazada)
   // en la lista representada por this alternativamente al principio y al final

   void engordar(EngordarLista & ch) {

       bool insertarAlInicio = true; 

       while (!ch.empty()) {

           // next es el siguiente nodo a engordar la lista

           Nodo* next = ch.fantasma->sig;           // dejamos la lista argumento en un estado correcto
           ch.fantasma->sig = next->sig;
           next->sig->ant = ch.fantasma;
           ch.nelems--;                             // eliminamos un elemento de la lista argumento
           this->nelems++;                          // añadimos un elemento a la lista  

           if (insertarAlInicio) {		           	// insertar al inicio

               Nodo* aux = this->fantasma->sig;     // guardamos el siguiente al fantasma (que sera el siguiente
                                                    //     al anadido tras la insercion)

               this->fantasma->sig = next;          // el nodo se anade en la primera posicion, correspondientemente
                                                    //  es el siguiente al fantasma

               next->sig = aux;                     // el nodo a annadir tiene como siguiente el nodo previamente guardado

               aux->ant = next;                     // el que anteriormente el primero, es ahora el segundo: su anterior
                                                    //      es el nodo recien insertado
               next->ant = this->fantasma;          // el nuevo primero tiene como anterior al fantasma
           }
           else {						            // insertar al final

               Nodo* aux = this->fantasma->ant;     // guardamos el anterior al fantasma (es decir, el ultimo
                                                    //  de nuestra lista)

               this->fantasma->ant = next;          // el nuevo ultimo es el nodo recien insertado

               next->ant = aux;                     // que a su vez tiene como anterior al nodo que era previamente el ultimo

               aux->sig = next;                     // (siendo el siguiente del penultimo)

               next->sig = this->fantasma;          // y como siguiente al fantasma
           }

           insertarAlInicio = !insertarAlInicio;
       }
   }
};


inline std::ostream& operator<<(std::ostream & o, EngordarLista const& lista) {
   lista.print(o);
   return o;
}

EngordarLista leerLista() {
   EngordarLista lista;
   int n, val;
   std::cin >> n; // tamaño
   while (n--) {
      std::cin >> val;
      lista.push_back(val);
   }
   return lista;
}


void resuelveCaso() {
   auto lista1 = leerLista();
   auto lista2 = leerLista();
   lista1.engordar(lista2);
   std::cout << lista1 << '\n';
}


int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.txt");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   
   int casos;
   cin >> casos;
   while (casos--) {
      resuelveCaso();
   }

   // para dejar todo como estaba al principio
#ifndef DOMJUDGE
   std::cin.rdbuf(cinbuf);
   system("PAUSE");
#endif
   return 0;
}
