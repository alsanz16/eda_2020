//
//  Lista.h
//  wip
//
//  Created by Álvaro on 24/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Lista_h
#define Lista_h

#include "queue.h"

template <typename T>
class insertable_queue : public queue<T> {
using Node = typename queue<T>::Nodo;
public:
    insertable_queue() {};
    void insert(insertable_queue<T> &q, int pos) {
        if (q.empty()) return;
        
        if (pos == 0) {
            q.ult->sig = this->prim;
            this->prim = q.prim;
        }
        else if (pos < this->nelems) {
            Node *current = this->prim;
            for (int i = 0; i < pos-1; i++) current = current -> sig;
            Node *aux = current->sig;
            current->sig = q.prim;
            q.ult->sig = aux;
        }
        else this->ult->sig = q.prim;
        
        this->nelems += q.nelems;
        q.prim = q.ult = nullptr;
        q.nelems = 0;
    
    }
};

#endif /* Lista_h */
