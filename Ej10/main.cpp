// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include "insertable_queue.h"

template <typename T>
void print(queue<T> q);

int main()
{
    int l1, l2, elementoLeido, pos;
    std::cin>>l1;
    
    while (std::cin) {
        insertable_queue<int> q1, q2;
        for (int i = 0; i < l1; i++) {
            std::cin >> elementoLeido;
            q1.push(elementoLeido);
        }
        std::cin >> l2 >> pos;
        for (int i = 0; i < l2; i++) {
            std::cin >> elementoLeido;
            q2.push(elementoLeido);
        }
        
        q1.insert(q2, pos);
        std::cout << q1  << std::endl;
        std::cin >> l1;
    }
}
