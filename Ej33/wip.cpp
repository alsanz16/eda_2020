#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

typedef struct {
    std::set<std::string> enviosCorrectos;
    int tiempo;
    std::map<std::string, int> intentos;
} tPuntuacionEquipo;

void resolverCaso() {
    std::map<std::string, tPuntuacionEquipo> puntos;
    std::string nombreEquipo, nombreProblema, resultado;
    int tiempo;

    std::cin >> nombreEquipo;
    while (nombreEquipo != "FIN") {
        std::cin >> nombreProblema >> tiempo >> resultado;
        auto& equipo = puntos[nombreEquipo];
        if (!equipo.enviosCorrectos.count(nombreProblema)) {
            if (resultado == "AC") {
                equipo.tiempo += 20 * equipo.intentos[nombreProblema] + tiempo;
                equipo.enviosCorrectos.insert(nombreProblema);
            }
            else ++equipo.intentos[nombreProblema];
        }
        std::cin >> nombreEquipo;
    }

    std::vector<std::string> equipos;
    for (auto it = puntos.begin(); it != puntos.end(); ++it) equipos.push_back(it->first);
    std::sort(equipos.begin(), equipos.end(),
        [&puntos](auto a, auto b) {
        return puntos[a].enviosCorrectos.size() > puntos[b].enviosCorrectos.size()
            || puntos[a].enviosCorrectos.size() == puntos[b].enviosCorrectos.size()
                && puntos[a].tiempo < puntos[b].tiempo
            || puntos[a].enviosCorrectos.size() == puntos[b].enviosCorrectos.size()
                && puntos[a].tiempo == puntos[b].tiempo && a < b; }
    );
    for (auto it = equipos.begin(); it != equipos.end(); ++it)
        std::cout << *it << " " << puntos[*it].enviosCorrectos.size() 
        << " " << puntos[*it].tiempo << "\n";
    std::cout << "---\n";
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; ++i) resolverCaso();

}