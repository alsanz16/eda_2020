//
//  Polinomio.h
//  wip
//
//  Created by Álvaro on 25/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Polinomio_h
#define Polinomio_h

#include <algorithm>
#include <math.h>
#include <vector>
#include <iostream>

template <class T>
class Polinomio {
private:
    std::vector<std::pair<T, T>> _pol;
    bool busquedaBinaria(std::pair<T,T> elem, int &pos) {
        int ini = 0, fin = _pol.size()-1;
        int medio = (ini + fin) / 2;
        bool encontrado = false;
        while (!encontrado && ini <= fin) {
            medio = (ini + fin) / 2;
            if (_pol[medio].first > elem.first) ini = medio + 1;
            else if (elem.first > _pol[medio].first) fin = medio - 1;
            else encontrado = true;
        }
        pos = encontrado ? medio : ini;
        return encontrado;
    };

public:
    void insert(std::pair<T,T> elem) {
        int pos;
        if (busquedaBinaria(elem, pos))
            _pol[pos].second += elem.second;
        else {
            _pol.push_back(std::pair<T,T>() );
            for (int i = _pol.size()-1; i > pos; i--)
                _pol[i] = _pol[i-1];
            _pol[pos] = elem;
        }
    };
    
    T evaluate(T x) {
        T result = 0;
        for (int i = 0; i < _pol.size(); i++)
            result += _pol[i].second * pow(x, _pol[i].first);
        return result;
    }
};

#endif /* Polinomio_h */
