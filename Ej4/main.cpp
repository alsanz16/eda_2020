// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <set>
#include <stack>
#include <string>
#include <vector>
#include "Polinomio.h"

int main() {
    int a, b, nEvaluar, x;
    std::cin >> a >> b;
    
    while (std::cin) {
        Polinomio<long long int> p;
        while (!(a == 0 && b==0)) {
            p.insert({b, a});
            std::cin >> a >> b;
        }
        std::cin >> nEvaluar;
        for (int j = 0; j < nEvaluar; j++) {
            std::cin >> x;
            std::cout << p.evaluate(x) << " ";
        }
        std::cout << std::endl;
        std::cin >> a >> b;
    }
}
