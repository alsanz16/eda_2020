#pragma once
//
//  conjunto.h
//

#ifndef conjunto_h
#define conjunto_h

#include <stdexcept>  // std::domain_error
#include <utility>    // std::move

template <class T>
class set {
public:
    set(int tam = TAM_INICIAL); // constructor
    set(set<T> const& other); // constructor por copia
    set<T>& operator=(set<T> const& other); // operador de asignación
    ~set(); // destructor
    T pop();
    void insert(T e);
    bool contains(T e) const;
    void erase(T e);
    bool empty() const;
    int size() const;
    void insertMayor(T e);
    T getMin();
private:
    const static int TAM_INICIAL = 2;
    int contador;
    int capacidad;
    T* datos;
    bool busquedaBinaria(T elem, int& pos);
    void amplia();
    void libera();
    void copia(set<T> const& other);
};

template <class T>
set<T>::set(int tam) : contador(0), capacidad(tam), datos(new T[capacidad]) {}

template <class T>
set<T>::~set() {
    libera();
}

template <class T>
void set<T>::libera() {
    delete[] datos;
}

/** Constructor por copia */
template <class T>
set<T>::set(set<T> const& other) {
    copia(other);
}

/** Operador de asignación */
template <class T>
set<T>& set<T>::operator=(set<T> const& other) {
    if (this != &other) {
        libera();
        copia(other);
    }
    return *this;
}

template <class T>
void set<T>::copia(set<T> const& other) {
    capacidad = other.capacidad;
    contador = other.contador;
    datos = new T[capacidad];
    for (int i = 0; i < contador; ++i)
        datos[i] = other.datos[i];
}

template <class T>
void set<T>::insert(T e) {
    int pos;
    bool contains = busquedaBinaria(e, pos);
    if (!contains ) {
        if (contador == capacidad) amplia();
        for (int i = contador; i > pos; i--) datos[i] = datos[i-1];
        ++contador;

        datos[pos] = e;
    }
}

template <class T>
void set<T>::insertMayor(T e) {
    int pos;
    bool contains = busquedaBinaria(e, pos);
    if (!contains && contador < capacidad) {
        for (int i = contador; i > pos; i--) datos[i] = datos[i - 1];
        contador++;
        datos[pos] = e;
    }
    else if (!contains && e > getMin()) {
        pop();
        insertMayor(e);
    }
}

template <class T>
T set<T>::pop() {
    if (contador > 0)
        return datos[--contador];
}

template <class T>
T set<T>::getMin () {
    if (contador > 0)
        return datos[contador-1];
}

template <class T>
bool set<T>::busquedaBinaria(T elem, int &pos) {
    int ini = 0, fin = contador - 1;
    int medio;
    bool encontrado = false;
    while (!encontrado && ini <= fin) {
        medio = (ini + fin) / 2;
        if (datos[medio] > elem) ini = medio + 1;
        else if (elem > datos[medio]) fin = medio - 1;
        else encontrado = true;
    }
    pos = encontrado ? medio : ini;
    return encontrado;
}

template <class T>
bool set<T>::contains(T e) const {
    return busquedaBinaria(e);
}

template <class T>
void set<T>::amplia() {
    T* nuevos = new T[2 * capacidad];
    for (int i = 0; i < capacidad; ++i)
        nuevos[i] = std::move(datos[i]);
    delete[] datos;
    datos = nuevos;
    capacidad *= 2;
}

template <class T>
void set<T>::erase(T e) {
    int i = 0;
    while (i < contador && datos[i] != e)
        ++i;
    if (i < contador) {
        datos[i] = datos[contador - 1];
        --contador;
    }
    else
        throw std::domain_error("El elemento no esta");
}


template <class T>
bool set<T>::empty() const {
    return contador == 0;
}

template <class T>
int set<T>::size() const {
    return contador;
}

#endif // conjunto_h
