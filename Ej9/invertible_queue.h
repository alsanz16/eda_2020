//
//  Lista.h
//  wip
//
//  Created by Álvaro on 24/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Lista_h
#define Lista_h

#include "queue.h"

template <typename T>
class invertible_queue : public queue<T> {
using Node = typename queue<T>::Nodo;
public:
    invertible_queue() {};
    void invert() {
        Node *current = this->prim;
        Node *next, *prev = nullptr;
        
        // aux1 guarda el elemento anterior en la lista, aux2 el siguiente
        // para cada nodo, lo enlazamos al elemento anterior
        std::swap(this->prim, this->ult);
        while (current != nullptr) {
            next = current->sig;
            current->sig = prev;
            prev = current;
            current = next;
        }
    }
};


#endif /* Lista_h */
