// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <set>
#include <stack>
#include <string>
#include <vector>
std::string transformar1(const std::string &s1);
std::string transformar2(const std::string &s2);
std::string transformar2inv(const std::string &s2);

int main()
{
    std::string mensaje;
    std::getline(std::cin, mensaje);
    while (std::cin) {
        std::cout << transformar1(transformar2inv(mensaje)) << std::endl;
        std::getline(std::cin, mensaje);
    }
}

std::string transformar1(const std::string &s1) {
    std::unordered_set<char> vocales = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};
    std::stack<char> letras;
    std::string nuevaString = "";
    for (char c: s1)
        if (vocales.count(c)) {
            while(!letras.empty()) {
                nuevaString += letras.top();
                letras.pop();
            }
            nuevaString += c;
        }
        else letras.push(c);
    while(!letras.empty()) {
        nuevaString += letras.top();
        letras.pop();
    }

    return nuevaString;
}

std::string transformar2(const std::string &s2) {
    int i = 0, j = s2.size()-1;
    std::string nuevaString = "";
    while (i < j) nuevaString = nuevaString + s2.at(i++) + s2.at(j--);
    if (j % 2 == 0) nuevaString += s2.at(i);
    return nuevaString;
}

std::string transformar2inv(const std::string &s2) {
    std::string nuevaString;
    for (int i = 0; i < s2.size(); i += 2) nuevaString += s2.at(i);
    for (int i = s2.size()-1-(s2.size()%2); i > 0; i -= 2) nuevaString += s2.at(i);
    return nuevaString;
}
