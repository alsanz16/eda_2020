#include "bintree_eda.h"
#include <iostream>
#include <fstream>
using namespace std;

typedef struct {
    bool esBinario;
    int altura;
} tInfo;

tInfo esBinario(bintree<int> const& arbol) {
    if (arbol.empty()) return { true, 0 };
    tInfo izq = esBinario(arbol.left());
    tInfo der = esBinario(arbol.right());
    bool esBinario = (arbol.left().empty() || arbol.root() >= arbol.left().root() + 18)
        && (arbol.right().empty() || arbol.root() >= arbol.right().root() + 18)
        && !(arbol.left().empty() && !arbol.right().empty())
        && (arbol.left().empty() || arbol.right().empty() || arbol.left().root() >= arbol.right().root() + 2);
    return { esBinario && izq.esBinario && der.esBinario, 1 + std::max(izq.altura, der.altura) };
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; i++) {
       tInfo info = esBinario(leerArbol(-1));
       std::cout << (info.esBinario ? "SI" : "NO") << " ";
       if (info.esBinario) std::cout << info.altura;
       std::cout << "\n";
   }
}