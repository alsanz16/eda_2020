#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>
using namespace std;

std::map<string, int> leerDiccionario(std::string& dicc);
void printSet(std::set<std::string>& set, char symbol);

template <class T, class K>
void checkDifferences(std::map<T, K> &map1, std::map<T, K> &map2) {
    auto it1 = map1.begin();
    auto it2 = map2.begin();

    std::set<std::string> cambiadas;
    std::set<std::string> nuevas;
    std::set<std::string> eliminadas;

    while (it1 != map1.end() && it2 != map2.end()) {
        if (it2->first > it1->first) {
            eliminadas.insert(it1->first);
            ++it1;
        }
        else if (it2->first < it1->first) {
            nuevas.insert(it2->first);
            ++it2;
        }
        else {
            if (it1->second != it2->second) cambiadas.insert(it1->first);
            ++it1, ++it2;
        }
    }
    while (it1 != map1.end()) { eliminadas.insert(it1->first); ++it1; }
    while (it2 != map2.end()) { nuevas.insert(it2->first); ++it2; }
    printSet(nuevas, '+');
    printSet(eliminadas, '-');
    printSet(cambiadas, '*');
    if (!nuevas.size() && !eliminadas.size() && !cambiadas.size()) 
        std::cout << "Sin cambios\n";
    std::cout << "---\n";
}

void printSet(std::set<std::string>& set, char symbol) {
    if (set.size()) {
        std::cout << symbol << " ";
        for (auto it = set.begin(); it != set.end(); ++it) std::cout << *it << " ";
        std::cout << "\n";
    }
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   string clave;
   char aux;
   int valor;

   std::cin >> numCasos;
   std::cin.get();
   for (int i = 0; i < numCasos; ++i) {
       std::string dicString;
       std::getline(std::cin, dicString);
       auto dic1 = leerDiccionario(dicString);
       std::getline(std::cin, dicString);
       auto dic2 = leerDiccionario(dicString);
       checkDifferences(dic1, dic2);
   }
}

std::map<string, int> leerDiccionario(std::string& dicc) {
    std::stringstream ss(dicc);
    std::string clave;
    int valor;
    std::map<string, int> dic;
    ss >> clave >> valor;
    while (ss) {
        dic[clave] = valor;
        ss >> clave >> valor;
    }
    return dic;
}