#ifndef Pelicula_h
#define Pelicula_h

#include "Hora.h"
#include <string>
class Pelicula {
    Hora _inicio;
    Hora _final;
    std::string _nombre;
    
public:
    Pelicula () {};
    Pelicula (std::string nombre, Hora inicio, Hora duracion) : _nombre(nombre), _inicio(inicio), _final(inicio + duracion) {};
    void set(std::string nombre, Hora inicio, Hora duracion) {
        _nombre = nombre;
        _inicio = inicio;
        _final = inicio + duracion;
    }
    bool const operator< (const Pelicula &p2) {
        return _final < p2._final || (_final == p2._final && _nombre < p2._nombre);
    }
    Hora const &fin() const { return _final; };
    std::string const &nombre() const {return _nombre;};
};

inline std::istream& operator>>(std::istream &in, Pelicula &p1) {
    Hora inicio, duracion;
    std::string titulo;
    in >> inicio >> duracion;
    std::getline(in, titulo);
    p1.set(titulo, inicio, duracion);
    return in;
}

inline std::ostream& operator<<(std::ostream& out, const Pelicula& p1) {
    out << p1.fin() << p1.nombre();
    return out;
}
#endif /* Pelicula_h */
