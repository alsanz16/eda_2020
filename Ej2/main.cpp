// Ejercicio1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include <algorithm>
#include <iostream>
#include <vector>
#include "Hora.h"
#include "Pelicula.h"

template<class T>
bool busquedaBinaria(std::vector<T>& v, T elem, int& pos);

int main()
{
    
    int casos;
    std::cin >> casos;
    while (casos != 0) {
        std::vector <Pelicula> v(casos);
        for (Pelicula &p: v) std::cin >> p;
        std::sort(v.begin(), v.end(), [](Pelicula &p1, Pelicula &p2) {return p1 < p2;});
        for (Pelicula &p: v) std::cout << p << "\n";
        std::cout << "---\n";
        std::cin >> casos;
    }
}
