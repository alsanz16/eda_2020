#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <string>
#include <vector>
using namespace std;

template <class T, class K>
T getOrDefault(std::map<T, K>& map, T elem, K def);

int resolverCaso(int num) {
    int leido;
    int max = 0;
    int actual = 0;
    std::map<int, int> emitidos;
    for (int i = 0; i < num; ++i) {
        std::cin >> leido;
        actual = std::max(actual, getOrDefault(emitidos, leido, -1) +1);
        emitidos[leido] = i;
        max = std::max(1 + i - actual, max);
    }
    return max;
}

template <class T, class K>
T getOrDefault(std::map<T, K>& map, T elem, K def) {
    if (map.count(elem)) return map[elem];
    else return def;
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos, caso;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; ++i) {
       std::cin >> caso;
       std::cout << resolverCaso(caso) << "\n";
   }
}