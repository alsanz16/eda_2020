// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <queue>
#include <unordered_set>
#include <set>
#include <stack>
#include <string>
#include <vector>
#include "Persona.h"
#include "list_eda.h"

int main() {
    Persona personaLeida;
    int minEdad, maxEdad, n;
    std::cin >> n >> minEdad >> maxEdad;
    while (!(n== 0 && minEdad == 0 && maxEdad == 0)) {
        list<Persona> l;
        for (int i = 0; i < n; i++) {
            std::cin >> personaLeida;
            l.push_back(personaLeida);
            l.remove_if([minEdad, maxEdad](Persona p){return !(minEdad <= p.edad() && p.edad() <= maxEdad);});
        }
        for (auto it = l.begin(); it != l.end(); ++it) std::cout << *it << std::endl;
        std::cout << "---" << std::endl;
        std::cin >> n >> minEdad >> maxEdad;
    }
}
