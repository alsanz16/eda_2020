//
//  Persona.h
//  wip
//
//  Created by Álvaro on 25/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Persona_h
#define Persona_h
#include <iostream>
#include <string>

class Persona {
private:
    std::string _nombre;
    int _edad;
public:
    Persona() {};
    Persona(std::string nombre, int edad) : _nombre(nombre), _edad(edad) {};
    void set(std::string nombre, int edad) {_nombre = nombre; _edad = edad;};
    int edad() const {return _edad;};
    std::string nombre() const {return _nombre; };
};

inline std::ostream& operator<<(std::ostream &out, const Persona &p) {
    out << p.nombre();
    return out;
}

inline std::istream& operator>>(std::istream &in, Persona &p) {
    int edad;
    char aux;
    std::string nombre;
    std::cin >> edad;
    std::cin.get(aux);
    std::getline(std::cin, nombre);
    p.set(nombre, edad);
    return in;
}

#endif /* Persona_h */
