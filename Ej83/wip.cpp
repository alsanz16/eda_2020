#include "bintree_eda.h"
#include <iostream>
#include <fstream>
using namespace std;

typedef struct {
    int altura;
    int maxAltura;
} tInfo;

tInfo alturaMayorCompleto(bintree<char> const& arbol) {
    if (arbol.empty()) return { 0 ,0 };
    tInfo izq = alturaMayorCompleto(arbol.left());
    tInfo der = alturaMayorCompleto(arbol.right());
    int altura = std::min(izq.altura, der.altura) + 1;
    return { altura, std::max(std::max(izq.maxAltura, der.maxAltura), altura) };
}

int main() {

   // ajustes para que cin extraiga directamente de un fichero
#ifndef DOMJUDGE
   std::ifstream in("casos.in");
   auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
   int numCasos;
   std::cin >> numCasos;
   for (int i = 0; i < numCasos; i++)
       std::cout << alturaMayorCompleto(leerArbol('.')).maxAltura << "\n";
}