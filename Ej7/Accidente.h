//
//  Accidente.h
//  wip
//
//  Created by Álvaro on 24/02/2020.
//  Copyright © 2020 Álvaro. All rights reserved.
//

#ifndef Accidente_h
#define Accidente_h
#include "Fecha.h"

class Accidente {
private:
    Fecha _fecha;
    int _muertos;
public:
    Accidente() : _fecha(), _muertos(0) {};
    Accidente(Fecha fecha, int muertos) : _fecha(fecha), _muertos(muertos) {};
    
    int muertos() { return _muertos; };
    Fecha fecha() { return _fecha; };
    void set(Fecha fecha, int muertos) {
        _fecha = fecha;
        _muertos = muertos;
    }
    
    bool operator<=(const Accidente &a1) {
        return _muertos <= a1._muertos;
    }
};

inline std::istream &operator>>(std::istream &in, Accidente &a1) {
    Fecha f1;
    int muertos;
    in >> f1 >> muertos;
    a1.set(f1, muertos);
    return in;
}

inline std::ostream &operator<<(std::ostream &out, Accidente &a1) {
    out << a1.fecha();
    return out;
}

#endif /* Accidente_h */
