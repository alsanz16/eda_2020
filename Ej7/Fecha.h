#include <iomanip>

class Fecha {
    private:
        int _dia, _mes, _anno;
    public:
    Fecha() : _dia(0), _mes(0), _anno(0) {};
    Fecha (int dia, int mes, int anno) : _dia(dia), _mes(mes), _anno(anno) {};
    
    void set(int dia, int mes, int anno) {
        _dia = dia;
        _mes = mes;
        _anno = anno;
    }
    int dia() const { return _dia; }
    int mes() const { return _mes; }
    int anno() const { return _anno; }
    bool operator<(const Fecha &f2) {
        return _anno < f2._anno ||
        (_anno == f2._anno && _mes < f2._mes) ||
        (_anno == f2._anno && _mes == f2._mes && _dia < f2._dia);
    }
    
    bool operator==(const Fecha& f2) {
        return _anno == f2._anno && _mes == f2._mes && _dia == f2._dia;
    }

};

inline std::istream& operator>>(std::istream &in, Fecha &f1) {
    int anno, mes, dia;
    char aux;
    in >> dia >> aux >> mes >> aux >> anno;
    f1.set(dia, mes, anno);
    return in;
    

}

inline std::ostream& operator<<(std::ostream& out, const Fecha& f1) {
    out << std::setfill('0') << std::setw(2)
        << f1.dia() << "/" << std::setw(2) << f1.mes()
        << "/" << std::setw(2) << f1.anno();
    return out;
}
