// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>
#include "Accidente.h"

int main()
{
    Accidente nuevoAccidente;
    int casos;
    std::cin >> casos;
    while (std::cin) {
        std::stack<Accidente> pila;
        for (int i = 0; i < casos; i++) {
            std::cin >> nuevoAccidente;
            while(!pila.empty() && pila.top() <= nuevoAccidente)
                pila.pop();
            if (pila.empty()) std::cout << "NO HAY" << std::endl;
            else std::cout << pila.top() << std::endl;
            pila.push(nuevoAccidente);
        }
        std::cout << "---" << std::endl;
        std::cin >> casos;
    }
}
